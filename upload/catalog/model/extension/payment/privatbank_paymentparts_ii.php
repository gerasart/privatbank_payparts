<?php
class ModelExtensionPaymentPrivatbankPaymentpartsIi extends Model {
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/privatbank_paymentparts_ii');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_privatbank_paymentparts_ii_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('payment_privatbank_paymentparts_ii_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('payment_privatbank_paymentparts_ii_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		
		if ($this->config->get('payment_privatbank_paymentparts_ii_product_allowed')) {
			$products = $this->cart->getProducts();
			foreach ($products as $product) {
				if (!in_array($product['product_id'], $this->config->get('payment_privatbank_paymentparts_ii_product_allowed'))) {
					$status = false;
				 }
			}		
		}

		if ($this->config->get('payment_privatbank_paymentparts_ii_min_total') > $total) {
			$status = false;
		}
		if ($this->config->get('payment_privatbank_paymentparts_ii_max_total') < $total) {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'privatbank_paymentparts_ii',
				'title'      => $this->language->get('text_title'),
				'title1'      => $this->language->get('text_title1'),
				'terms'      => '',
				'sort_order' => $this->config->get('payment_privatbank_paymentparts_ii_sort_order')
			);
		}
        
		return $method_data;
	}
}
