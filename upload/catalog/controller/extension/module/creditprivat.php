<?php
class ControllerExtensionModuleCreditprivat extends Controller {

	 public function index(){
		 
        if (isset($this->request->get['product_id'])) {
            $product_id = $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }
		
		$thiscart = $this->cart->getProducts();
		

		$data['status_pp'] = false;
		$data['status_ii'] = false;
		$thiscartlast = end($thiscart);
//		$data['total'] = $thiscartlast['price'];
		$data['total'] = (int)$this->currency->format($this->tax->calculate($thiscartlast['price'], $thiscartlast['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
		
		$this->session->data['payment_privatbank_paymentparts_pp_sel_total'] = $data['total'];

		if ($this->config->get('payment_privatbank_paymentparts_pp_status') == 1) {
			
			if ((!$this->config->get('payment_privatbank_paymentparts_pp_product_allowed'))	|| ($this->config->get('payment_privatbank_paymentparts_pp_product_allowed') && in_array($product_id, $this->config->get('payment_privatbank_paymentparts_pp_product_allowed')))) {
			
				if ( ($this->config->get('payment_privatbank_paymentparts_pp_min_total') <= $data['total']) && (($this->config->get('payment_privatbank_paymentparts_pp_max_total')) >= $data['total'])) {

					$data['status_pp'] = true;
				}
			}
		}
		
		if ($this->config->get('payment_privatbank_paymentparts_ii_status') == 1) {
			
			if ((!$this->config->get('payment_privatbank_paymentparts_ii_product_allowed'))	|| ($this->config->get('payment_privatbank_paymentparts_ii_product_allowed') && in_array($product_id, $this->config->get('payment_privatbank_paymentparts_ii_product_allowed')))) {
			
				if ( ($this->config->get('payment_privatbank_paymentparts_ii_min_total') <= $data['total']) && (($this->config->get('payment_privatbank_paymentparts_ii_max_total')) >= $data['total'])) {

					$data['status_ii'] = true;
				}
			}
		}

		$data['partsCountpp'] = (!$this->config->get('payment_privatbank_paymentparts_pp_paymentquantity') ? '12' : $this->config->get('payment_privatbank_paymentparts_pp_paymentquantity'));
		$data['partsCountii'] = (!$this->config->get('payment_privatbank_paymentparts_ii_paymentquantity') ? '12' : $this->config->get('payment_privatbank_paymentparts_ii_paymentquantity'));

		$data['markuppp'] = (!$this->config->get('payment_privatbank_paymentparts_pp_markup') ? '1' : $this->config->get('payment_privatbank_paymentparts_pp_markup'));
		$data['markupii'] = (!$this->config->get('payment_privatbank_paymentparts_ii_markup') ? '1' : $this->config->get('payment_privatbank_paymentparts_ii_markup'));

		$this->response->setOutput($this->load->view('extension/module/creditprivat', $data));
    }
}