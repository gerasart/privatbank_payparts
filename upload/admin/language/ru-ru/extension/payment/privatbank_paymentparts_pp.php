<?php
// Heading
$_['heading_title']    					= 'Оплата частями (ПриватБанк)';

//Text
$_['text_module']						= 'Оплата';
$_['text_extension']	 = 'Платежи';
$_['text_success']						= 'Настройки модуля успешно обновлены!';
$_['text_edit']							= 'Редактирование модуля';
$_['text_privatbank_paymentparts_pp'] 	= '<a target="_BLANK" href="https://payparts2.privatbank.ua/ipp/"><img src="view/image/payment/pbPayPat.png" alt="Privatbank Paymentparts" title="Privatbank Paymentparts" style="border: 1px solid #EEEEEE;width: 56px;height: 35px" /></a>';
$_['text_paymentparts_url']				= '<p for="input-shop-id">Для получения идентификатора и пароля, необходимо зарегистрировать магазин в <a target="_BLANK" href="https://payparts2.privatbank.ua/ipp/">Личном кабинете Приват банка</a></p>';

//Entry
$_['entry_shop_id']						= 'Идентификатор магазина:';
$_['entry_shop_password']				= 'Пароль магазина:';
$_['entry_paymentquantity']				= 'Количество платежей:';
$_['entry_markup']						= 'Коэффициент наценки:';
$_['entry_min_total']					= 'Минимальная цена:';
$_['entry_max_total']					= 'Максимальная цена:';
$_['entry_status']						= 'Статус:';
$_['entry_sort_order']					= 'Порядок сортировки:';
$_['entry_geo_zone']    				= 'Географическая зона:';

//Payment State order Statuses
$_['entry_completed_status']    		= 'Статус "платеж успешно совершен"';
$_['entry_failed_status']       		= 'Статус "ошибка при создании платежа"';
$_['entry_canceled_status']     		= 'Статус "платеж отменен (клиентом)"';
$_['entry_rejected_status']     		= 'Статус "платеж отклонен"';
$_['entry_clientwait_status']   		= 'Статус "ожидание"';
$_['entry_created_status']      		= 'Статус "платеж создан"';

//Error
$_['error_permission'] 					= 'У Вас нет прав для управления этим модулем!';
$_['error_shop_id']						= 'Необходимо ввести ID магазина!';
$_['error_shop_password']				= 'Необходимо ввести пароль магазина!';
$_['error_paymentquantity']				= 'Необходимо указать максимальное количество платежей, не более 24!';

//Payment order status tab
$_['tab_order_status']					= 'Статус Заказа';

//Help
$_['help_paymentquantity']				= 'Введите максимальное количество платежей';
$_['help_markup']						= 'Если необходимо, установите наценку на стоимость товара, например 1.027';
$_['help_allowed']						= "Если необходимо ограничить доступные для оформления товары, выберите их тут";
$_['help_min_total']					= "Введите минимальную стоимость товара(ов), выше которой будет доступна рассрочка";
$_['help_max_total']					= "Введите максимальную стоимость товара(ов), ниже которой будет доступна рассрочка";
