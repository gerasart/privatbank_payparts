$('body').on('click', '#activatescript', function(){
    initLocale();
    // calculation is performed when any slider or .numeric-input changes it's value
    initElements();
    calcChange(); // initial calculation
});
var initLocale = function () {
        //var curLoc = window.navigator.language || navigator.language;
        if (lang[curLang] == undefined) {
            curLang = 'RU';
        }
        $("#stPpTitle").html(lang[curLang].stpp_title);
        $("#ipTitle").html(lang[curLang].ip_title);
        $("#ppdpTitle").html(lang[curLang].ppdp_title);
        $("#ppTitle").html(lang[curLang].pp_title);

        $(".holder-phys").find('span').html(lang[curLang].amount);
        $(".holder-jur").find('span').html(lang[curLang].compensation);

        $(".result-additional-data-phys").text(lang[curLang].hrn_per_month);
        $(".result-additional-data-jur").text(lang[curLang].hrn);

        var term = $("#term");
        term.find(".legend").html(lang[curLang].term);
        term.find(".additional-data").html(lang[curLang].month);

        var term = $("#term2");
        term.find(".legend").html(lang[curLang].term);
        term.find(".additional-data2").html(lang[curLang].month);

        $("#paymentsCount").html("2");
        $(".payments").html(lang[curLang].payments);

        var price = $("#price");
        price.find(".legend").html(lang[curLang].price);
        price.find(".additional-data").html(lang[curLang].hrn);
        $(".showRateSheet").html(lang[curLang].showTariff);
        $("#printRateSheet").html(lang[curLang].printRateSheet);
};

var initElements = function () {
    // term slider
    initTermSlider();
	initTermSlider2();

    // price slider
    initPriceSlider();

    // inputs
    $(".numeric-input").keypress(function (evt) {
        var theEvent = evt || window.event;
        var code = theEvent.keyCode || theEvent.which;
        if(code == 37 || code == 39 || code == 8) {
            return;
        }
        var key = String.fromCharCode( code );
        var regex = /[0-9]/;
        if(!regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }

    });
    $(".numeric-input").keyup(function () {
            $(this).trigger('update');

    });


    initTermInput();
	initTermInput2();

    initPriceInput();
};

var initTermSlider = function () {
    $("#termSlider").slider({
        value: constants.termMin,
        max: constants.termMax,
        min: constants.termMin,
        step: constants.termStep,
        slide: function (event, ui) {
            sliderMoved($(this), $("#termInput"), $("#termProgress"), ui.value);
        }
    });
};

var initTermSlider2 = function () {
    $("#termSlider2").slider({
        value: constants.termMin2,
        max: constants.termMax2,
        min: constants.termMin2,
        step: constants.termStep2,
        slide: function (event, ui) {
            sliderMoved($(this), $("#termInput2"), $("#termProgress2"), ui.value);
        }
    });
};

var initPriceSlider = function () {
    $("#priceSlider").slider({
        value: constants.priceMin,
        max: constants.priceMax,
        min: constants.priceMin,
        step: constants.priceStep,
        slide: function (event, ui) {
            sliderMoved($(this), $("#priceInput"), $("#priceProgress"), ui.value);
        }
    });
};

var initTermInput = function () {
    var $inp = $("#termInput");
    $inp.attr("min", constants.termMin);
    $inp.attr("max", constants.termMax);
    $inp.val(constants.termMin);

    $inp.on('update', function () {
        inputChanged($inp, $("#termSlider"), $("#termProgress"));
    });

};

var initTermInput2 = function () {
    var $inp = $("#termInput2");
    $inp.attr("min", constants.termMin2);
    $inp.attr("max", constants.termMax2);
    $inp.val(constants.termMin2);

    $inp.on('update', function () {
        inputChanged($inp, $("#termSlider2"), $("#termProgress2"));
    });

};

var initPriceInput = function () {
    var $inp = $("#priceInput");
    $inp.attr("min", constants.priceMin);
    $inp.attr("max", constants.priceMax);
    $inp.val(constants.priceInitial);

    $inp.on('update', function () {
        inputChanged($inp, $("#priceSlider"), $("#priceProgress"));
    });

};
/////////////
var sliderMoved = function (slider, inputToChange, progressToChange, newValue) {
    var sMax = slider.slider("option", "max");
    var sMin = slider.slider("option", "min");
    inputToChange.val(newValue);

    var progress = (newValue - sMin) * 100 / ( sMax - sMin );
    progressToChange.css('width', progress + "%");
    calcChange();
};

var inputChanged = function (input, slider, progressToChange) {
    validateInput(input);
    var newVal = input.val();
    slider.slider("value", newVal);
    var sMax = slider.slider("option", "max");
    var sMin = slider.slider("option", "min");
    var progress = (newVal - sMin) * 100 / ( sMax - sMin );
    progressToChange.css('width', progress + "%");
    calcChange();
};

var validateInput = function (input) {
    var min = parseInt(input.attr("min"), 10);
    var max = parseInt(input.attr("max"), 10);
    var val = parseInt(input.val(), 10);
    if (val < 1) input.val(min); // or client won't have a chance to enter number < 10
    if (val > max) input.val(max);
};

var calcChange = function(){
    if( $('#stPpResult').html() )
        return calcJur();
    else
        return calcPhys();
};

var calcPhys = function(){
    var resCalc = PP_CALCULATOR.calculatePhys($('#termInput').val(), $('#priceInput').val());
	var resCalc2 = PP_CALCULATOR.calculatePhys($('#termInput2').val(), $('#priceInput2').val());
    $('#paymentsCount').html(resCalc.payCount);
	$('#paymentsCount2').html(resCalc2.payCount);
    $('#ipResultValue').html(resCalc.ipValue);
    $('#ppResultValue').html(resCalc2.ppValue);
};

var calcJur = function(){
    var resCalc = PP_CALCULATOR.calculateJur($('#termInput').val(), $('#priceInput').val());
	var resCalc2 = PP_CALCULATOR.calculateJur($('#termInput2').val(), $('#priceInput2').val());

    if(resCalc.ppValue == 0.00 && resCalc.ppValueHint == '0.00')
        $('#ppResult').addClass("disabled");
    else
        $('#ppResult').removeClass("disabled");

    $('#ppResultHint').html(resCalc.ppValueHint);
    $('#stPpResultValue').html(resCalc.stPpValue);
    $('#paymentsCount').html(resCalc.payCount);
    $('#ipResultValue').html(resCalc.ipValue);
    $('#ppResultValue').html(resCalc2.ppValue);
};
var lang = {
    ru: {
        stpp_title: 'Оплата частями<br/>(стандартная)',
        ppdp_title: 'Оплата частями<br/>(деньги в периоде)',
        pp_title: '«Оплата частями»',
        ip_title: '«Мгновенная рассрочка»',
        amount: 'Сумма платежа',
        hrn_per_month: 'грн / мес',
        term: 'Срок кредитования',
        price: 'Стоимость товара',
        month: 'месяцев',
        payments: 'платежей',
        hrn: 'гривен',
        compensation: 'Сумма возмещения',
        showTariff: 'Посмотреть тарифную сетку',
        printRateSheet: 'Распечатать'
    },
    uk: {
        stpp_title: 'Оплата частинами<br/>(стандартна)',
        ppdp_title: 'Оплата частинами<br/>(гроші в періоді)',
        pp_title: '«Оплата частинами»',
        ip_title: '«Миттєва розстрочка»',
        amount: 'Сума платежу',
        hrn_per_month: 'грн/міс',
        term: 'Строк кредитування',
        price: 'Вартість товару',
        month: 'місяців',
        payments: 'платежів',
        hrn: 'Гривень',
        compensation: 'Сума відшкодування',
        showTariff: 'Переглянути тарифи',
        printRateSheet: 'Роздрукувати'
    }
};



/**
 * Created by anton on 13.07.15.
 * Example :
 *          var resCalc = PP_CALCULATOR.calculatePhys(1, 500);
 *          resCalc = {payCount: 2, ipValue: "264.50", ppValue: "250.00"}

 *          var resCalc = PP_CALCULATOR.calculateJur(1, 500);
 *          resCalc = {payCount: 2, stPpValue: "469.50", ipValue: "491.00", ppValue: "483.50", ppValueHint: "233.50 + 1*250.00"}
 *
 *          var resCalc = PP_CALCULATOR.calculateJur(6, 500);
 *          resCalc = {payCount: 7, stPpValue: "418.00", ipValue: "491.00", ppValue: "0.00", ppValueHint: "0.00"}
 */
var PP_CALCULATOR = (function () {
    var my = {};
    // Комиссия
    var commissions = {
        ipCommission: 2.9, // Для физ.
        acqCommission: 0.02, // Для юр., с суммы покупки
        ppCommission: 0.015 // Для юр.
    };

    function privParseInt(num) {
        return parseInt(num, 10)
    }

    function getValByTerm(term) {
        var commissions = {
            1: 0.043,
            2: 0.07,
            3: 0.089,
            4: 0.113,
            5: 0.135,
            6: 0.146,
            7: 0.166,
            8: 0.186,
            9: 0.205,
            10: 0.225,
            11: 0.241,
            12: 0.239,
            13: 0.255,
            14: 0.27,
            15: 0.285,
            16: 0.3,
            17: 0.315,
            18: 0.33,
            19: 0.341,
            20: 0.355,
            21: 0.3675,
            22: 0.38,
            23: 0.391,
            24: 0.403
        };
        return commissions[term];
    }

    /* Вычисление платежей для физ.лиц
    * Входные параметры: количество месяцев, цена
    * Исходящие параметры: количество платежей,
    *                      мгновенная рассрочка(грн/мес),
     *                     оплата частями(грн/мес)
    * */
    my.calculatePhys = function (paymentsCount, price) {
        if (isNaN(paymentsCount) || isNaN(price)) return;

        paymentsCount = privParseInt(paymentsCount) + 1;
        price = privParseInt(price);

        var ip = price / paymentsCount + price * (commissions.ipCommission / 100);
        var pp = price / paymentsCount;

        return ({
            payCount:paymentsCount, // Количество платежей
            ipValue:ip.toFixed(2),  // Мгновенная рассрочка, грн/мес
            ppValue:pp.toFixed(2)   // Оплата частями, грн/мес
        });
    };
    /* Вычисление платежей для юр.лиц
     * Входные параметры: количество месяцев, цена
     * Исходящие параметры: количество платежей,
     *                      оплата частями (стандартная),
     *                      мгновенная рассрочка,
     *                      оплата частями(деньги в периоде),
     *                      оплата частями(деньги в периоде) - подробно о платеже
     * */
    my.calculateJur = function (paymentsCount, price) {
        if (isNaN(paymentsCount) || isNaN(price)) return;
        paymentsCount = privParseInt(paymentsCount) + 1;
        price = privParseInt(price);

        <!-- Оплата частями стандартная -->
        var tabVal = getValByTerm(paymentsCount - 1);
        var stpp = price * (1 - (tabVal + commissions.acqCommission));

        <!-- Оплата частями деньги в пер-->
        var pp = 0;
        var ppValHint = '0.00';


            var singlePayment = price / paymentsCount;
            var ppFirst = singlePayment - price * (commissions.acqCommission + commissions.ppCommission);
            var ppSecond = singlePayment;
            var ppOther = (paymentsCount - 1) * ppSecond;
            pp = ppFirst + ppOther;
            ppValHint = ppFirst.toFixed(2) + " + " + (paymentsCount-1) + "*" + ppSecond.toFixed(2);


        <!-- Мгновенная рассрочка -->
        var ip = price * (1 - commissions.acqCommission);

        return ({
            payCount:paymentsCount, // Количество платежей
            stPpValue:stpp.toFixed(2), // Сумма возмещения : Оплата частями (стандартная)
            ipValue:ip.toFixed(2),  // Сумма возмещения : Мгновенная рассрочка
            ppValue:pp.toFixed(2),  // Сумма возмещения : Оплата частями(деньги в периоде)
            ppValueHint:ppValHint   // Оплата частями(деньги в периоде), подробно о платеже(формула суммы возмещения)
        });
    };

    return my;
}());
