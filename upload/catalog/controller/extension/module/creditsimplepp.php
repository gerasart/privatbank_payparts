<?php
class ControllerExtensionModuleCreditSimplepp extends Controller {

	public function index(){
		 
		$this->language->load('payment/payment_privatbank_paymentparts_pp');

		$data['button_confirm'] = $this->language->get('button_confirm');
        $data['text_label_partsCount'] = $this->language->get('text_label_partsCount');
        $partsCount = $this->config->get('payment_privatbank_paymentparts_pp_paymentquantity');
        $partsCountArr = array();
        for($i=$partsCount;$i>=2;$i--){
            $partsCountArr[] = $i;           
        }
        $data['partsCounts'] = $partsCountArr;
		
		
		if (isset ($this->session->data['payment_privatbank_paymentparts_pp_sel'])) {
			$data['partsCountSel'] = $this->session->data['payment_privatbank_paymentparts_pp_sel'];
		}else {
			$data['partsCountSel'] = '';
		}		
        
		if (!$this->config->get('payment_privatbank_paymentparts_pp_test')) {
            $data['action'] = $this->url->link('extension/payment/payment_privatbank_paymentparts_pp/sendDataDeal', '', 'SSL');
		} else {
			$data['action'] = 'https://brtp.test.it.loc/ipp/';
		}

		$this->response->setOutput($this->load->view('extension/module/credit_simple_pp', $data));
    }
}